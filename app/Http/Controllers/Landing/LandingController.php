<?php

namespace App\Http\Controllers\Landing;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

use Auth;

use App\Models\Services;
use App\Models\Tagline;
use App\Models\AdvantageService;
use App\Models\AdvantageUser;
use App\Models\ThumbnailService;
use App\Models\Order;
use App\Models\User;
use App\Models\ExperienceUser;
use App\Models\DetailUser;



class LandingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $services = Services::orderBy('created_at', 'desc')->get();
        return view('pages.landing.index', compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      return abort(404);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      return abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      return abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      return abort(404);
    }


    // custom

    public function explore()
    {
      $services = Services::orderBy('created_at', 'desc')->get();
      return view('pages.landing.explore', compact('services'));
    }

    public function detail($id)
    {
      $service = Services::where('id', $id)->first();
      $thumbnail = ThumbnailService::where('service_id', $id)->get();
      $advantage_user = AdvantageUser::where('service_id', $id)->get();
      $advantage_service = AdvantageService::where('service_id', $id)->get();
      $tagline = Tagline::where('service_id', $id)->get();

      $detail_user = DetailUser::where('users_id', $service->users_id)->first();
      $experience_user = ExperienceUser::where('detail_user_id',$detail_user->id)->get();

      return view('pages.landing.detail', compact('service', 'thumbnail', 'advantage_user', 'advantage_service', 'tagline', 'experience_user'));
    }

    public function booking($id)
    {
      $service = Services::where('id', $id)->first();
      $user_buyer = Auth::user()->id;

      // Validation
      if ($service->users_id == $user_buyer) {
        toast()->warning('Sorry', 'Members cannot book their own service!');
        return back();
      }

      $order = new Order;
      $order->service_id = $service->id;
      $order->buyer_id = $user_buyer;
      $order->freelance_id = $service->user->id;    //eloquent one no many at Model Service
      $order->file = NULL;
      $order->note = NULL;
      $order->expired = date('Y-m-d', strtotime('+3 days'));
      $order->order_status_id = 4;
      $order->save();

      //redirectto detail order
      $order_detail = Order::where('id', $order->id)->first();

      return redirect()->route('detail.booking.landing', $order->id);

    }

    public function detail_booking($id)
    {

      $order = Order::where('id', $id)->first();

      return view('pages.landing.booking', compact('order'));

    }
}
