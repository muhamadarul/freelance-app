<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Dashboard\Profile\UpdateDetailUserRequest;
use App\Http\Requests\Dashboard\Profile\UpdateProfileRequest;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

use File;
use Auth;


use App\Models\User;
use App\Models\DetailUser;
use App\Models\ExperienceUser;


class ProfileController extends Controller
{

    public function __construct()
    {
      $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::where('id', Auth::user()->id)->first();
        $experienceUser = ExperienceUser::where('detail_user_id', $user->detail_user->id)
                                          ->orderBy('id','asc')
                                          ->get();
        return view('pages.dashboard.profile', compact('user', 'experienceUser'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      return abort(404);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      return abort(404);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProfileRequest $request_profile, UpdateDetailUserRequest $request_detail_user)
    {
        $data_profile     = $request_profile->all();
        $data_detail_user = $request_detail_user->all();

        //get Photo
        $get_photo = DetailUser::where('users_id', Auth::user()->id)->first();

        //detele old photo
        if (isset($data_detail_user['photo'])) {
          $data =  'storage/'.$get_photo['photo'];
            if (File::exists($data)) {
              File::delete($data);
            }else {
              //detail delete
              File::delete('storage/app/public/'.$get_photo['photo']);
            }
        }


        //store photo to storage
        if (isset($data_detail_user['photo'])) {
          $data_detail_user['photo'] = $request_detail_user->file('photo')
                                                           ->store('assets/images', 'public');
        }

        //proccess saving/updating to users
        $user = User::find(Auth::user()->id);
        $user->update($data_profile);

        //proccess saving/updating to detail user
        $detail_user = DetailUser::find($user->detail_user->id);
        $detail_user->update($data_detail_user);

        //proccess saving/updating to experience user
        $experienceUserId = ExperienceUser::where('detail_user_id',$detail_user->id)->first();
        if (isset($experienceUserId)) {

          foreach ($data_profile['experience'] as $key => $value) {
              $experienceUser = ExperienceUser::find($key);
              $experienceUser->detail_user_id = $detail_user['id'];
              $experienceUser->experience = $value;
              $experienceUser->save();
          }
          //endforeach

        }else{

          foreach ($data_profile['experience'] as $key => $value) {

            if (isset($value)) {
              $experienceUser = new ExperienceUser;
              $experienceUser->detail_user_id = $detail_user['id'];
              $experienceUser->experience = $value;
              $experienceUser->save();
            }
            //endif
          }
          //endforeach
        }
        //end if else

        //alert
        toast()->success('Update Has Been Success');
        return back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      return abort(404);
    }

    // custom

    public function delete()
    {
      //get User Photo
      $get_user_photo = DetailUser::where('users_id', Auth::user()->id)->first();
      $path_photo = $get_user_photo['photo'];

      // second update value to null
      $data = DetailUser::find($get_user_photo['id']);
      $data->photo = NULL;
      $data->save();

      //delete file photo on storage
      $path = 'storage/'.$path_photo;
      if (File::exists($path)) {

        File::delete($path);

      }else {
        //detail delete
        File::delete('storage/app/public/'.$path_photo);

      }
      //endif

      //alert
      toast()->success('Delete Has Been Success');
      return back();

    }

}
