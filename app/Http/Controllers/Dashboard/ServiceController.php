<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Dashboard\Service\StoreServiceRequest;
use App\Http\Requests\Dashboard\Service\UpdateServiceRequest;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

use File;
use Auth;

use App\Models\Services;
use App\Models\Tagline;
use App\Models\AdvantageService;
use App\Models\AdvantageUser;
use App\Models\ThumbnailService;
use App\Models\Order;
use App\Models\User;





class ServiceController extends Controller
{

    public function __construct()
    {
      $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Services::where('users_id', Auth::user()->id)
                            ->orderBy('created_at', 'desc')
                            ->get();
        return view('pages.dashboard.service.index', compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.dashboard.service.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreServiceRequest $request)
    {
      $data = $request->all();
      $data['users_id'] = Auth::user()->id;

      //add to services
      $service = Services::create($data);

      //add to AdvantageService
      foreach ($data['advantage-service'] as $key => $value) {
        $advantages_service = new AdvantageService;
        $advantages_service->service_id = $service->id;
        $advantages_service->advantage = $value;
        $advantages_service->save();
      }

      //add to AdvantageUser
      foreach ($data['advantage-user'] as $key => $value) {
        $advantages_service = new AdvantageUser;
        $advantages_service->service_id = $service->id;
        $advantages_service->advantage = $value;
        $advantages_service->save();
      }

      //add to thumbnailService
      if ($request->hasFile('thumbnail')) {
        foreach ($request->file('thumbnail') as $file) {

          $path = $file->store(
            'assets/service/thumbnail', 'public'
          );

          $thumbnail_service = new ThumbnailService;
          $thumbnail_service->service_id = $service->id;
          $thumbnail_service->thumbnail = $path;
          $thumbnail_service->save();

        }//endforeach

      }//endif

      //add to Tagline
      foreach ($data['tagline'] as $key => $value) {
        $tagline = new Tagline;
        $tagline->service_id = $service->id;
        $tagline->tagline = $value;
        $tagline->save();
      }

      //alert with redirect to index service
      toast()->success('Save has been success');
      return redirect()->route('member.service.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Services $service)//parameter Model Service
    {
      // $services          = Services::where('id', $service['id'])->first();
      $advantage_service = AdvantageService::where('service_id', $service['id'])->get();
      $tagline           = Tagline::where('service_id', $service['id'])->get();
      $advantage_user    = AdvantageUser::where('service_id', $service['id'])->get();
      $thumbnail_service = ThumbnailService::where('service_id', $service['id'])->get();

      return view('pages.dashboard.service.edit', compact('service','advantage_service', 'tagline', 'advantage_user', 'thumbnail_service'));
      // return $services;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateServiceRequest $request, Services $service)
    {
        $data = $request->all();
        // dd($data);
        //update to service
        // $services = Services::find();
        $service->update($data);

        //update advantage service
        foreach ($data['advantage-services'] as $key => $value) {
          $advantage_service = AdvantageService::find($key);
          $advantage_service->advantage = $value;
          $advantage_service->save();
        }

        //add new advantage service
        if (isset($data['advantage-service'])) {
          $advantage_service = New AdvantageService;
          $advantage_service->service_id = $service['id'];
          $advantage_service->advantage = $value;
          $advantage_service->save();
        }

        //update advantage user
        foreach ($data['advantage-users'] as $key => $value) {
          $advantage_user = AdvantageUser::find($key);
          $advantage_user->advantage = $value;
          $advantage_user->save();
        }

        //add new advantage user
        if (isset($data['advantage-user'])) {
          $advantage_user = New AdvantageUser;
          $advantage_user->service_id = $service['id'];
          $advantage_user->advantage = $value;
          $advantage_user->save();
        }

        //update tagline
        foreach ($data['taglines'] as $key => $value) {
          $tagline = Tagline::find($key);
          $tagline->tagline = $value;
          $tagline->save();
        }

        //add new tagline
        if (isset($data['tagline'])) {
          $tagline = New Tagline;
          $tagline->service_id = $service['id'];
          $tagline->tagline = $value;
          $tagline->save();
        }

        //update thumbnail service
        if ($request->hasFile('thumbnails')) {
          foreach ($request->file('thumbnails') as $key => $file) {
            //get old photo
            $get_thumbnail = ThumbnailService::where('id', $key)->first();

            //store thumbnail
            $path = $file->store(
              'assets/service/thumbnail', 'public'
            );

            //update thumbnail
            $thumbnail_service = ThumbnailService::find($key);
            $thumbnail_service->thumbnail = $path;
            $thumbnail_service->save();

            //delete old photo
            $data = 'storage/'.$get_thumbnail['thumbnail'];
            if (File::exists($data)) {
              File::delete($data);
            }else {
              File::delete('storage/app/public/'.$get_thumbnail['thumbnail']);
            }

          }
        }

        //add new to thumbnail service
       if($request->hasFile('thumbnail')) {
          foreach ($request->file('thumbnail') as $file) {
            $path = $file->store(
              'assets/service/thumbnail', 'public'
            );

            $thumbnail_service = New ThumbnailService;
            $thumbnail_service->service_id = $service['id'];
            $thumbnail_service->thumbnail = $path;
            $thumbnail_service->save();
          }
       }

       toast()->success('Update has been success');
       return redirect()->route('member.service.index');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return abort(404);
    }
}
