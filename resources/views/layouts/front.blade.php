<!DOCTYPE html>
<html lang="{{ str_replace('_','-', app()->getLocale() ) }}" dir="ltr">

  <head>
    @include('includes.landing.meta')

    <title>@yield('title') | Serv </title>

    @stack('before-style')

    @include('includes.landing.style')

    @stack('after-style')

    {{-- @include('includes.landing.header-script') --}}


  </head>

  <body class="antialiased">

    <div class="relative">

      @include('includes.landing.header')

        @include('sweetalert::alert')

        @yield('content')

      @include('includes.landing.footer')

      @stack('before-script')

      @include('includes.landing.script')

      @stack('after-script')

      <!-- modal -->
      @include('components.modal.login')
      @include('components.modal.register-success')
      @include('components.modal.register')


    </div>

  </body>

</html>
